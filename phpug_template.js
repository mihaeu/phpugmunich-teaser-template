#!/usr/bin/env node
const {docopt} = require('docopt');
doc = `
Usage:
  phpug_template.js <date>

Arguments:
  date     Date of the meetup in YYYY_MM_DD format
`;
const args = docopt(doc);
const date = args['<date>'];

const puppeteer = require('puppeteer');
(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  page.setViewport({width: 1200, height: 675});
  await page.goto(`file://${__dirname}/${date}/index.html`);
  await page.screenshot({path: `${__dirname}/${date}/phpug_teaser_${date}.png`});

  await browser.close();
})();
