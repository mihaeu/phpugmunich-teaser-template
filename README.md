# PHP User Group Munich - Teaser Template

## Installation

```bash
git clone git@gitlab.com:mihaeu/phpugmunich-teaser-template.git
cd phpugmunich-teaser-template
yarn
# or if yarn is unavailable: npm install
```

## Usage

 - copy the template folder to a new folder with the proper name `YYYY_MM_DD`
 - add speaker photos and host logo
 - replace date, title, authors, host in `index.html`
 - run `./phpug_template.js YYYY_MM_DD`
 - upload `YYYY_MM_DD/phpug_teaser_YYYY_MM_DD.png` to meetup.com and announce the meetup, tweet on Twitter and send an internal email

## History

### 2020-01-22
![](2020-01-22/phpug_teaser_2020-01-22.png)

### 2019_10_23
![](2019_10_23/phpug_teaser_2019_10_23.png)

### 2019_07_24
![](2019_07_24/phpug_teaser_2019_07_24.png)

### 2019_05_29
![](2019_05_29/phpug_teaser_2019_05_29.png)

### 2019_03_27
![](2019_03_27/phpug_teaser_2019_03_27.png)

## License

See [`LICENSE`](LICENSE)
